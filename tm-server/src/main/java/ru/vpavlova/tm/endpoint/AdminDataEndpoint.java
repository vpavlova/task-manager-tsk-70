package ru.vpavlova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.api.endpoint.IAdminDataEndpoint;
import ru.vpavlova.tm.api.service.IBackupService;
import ru.vpavlova.tm.api.service.dto.ISessionService;
import ru.vpavlova.tm.component.Backup;
import ru.vpavlova.tm.dto.Session;
import ru.vpavlova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    private Backup backup;

    @Autowired
    private IBackupService backupService;

    @Autowired
    private ISessionService sessionService;

    @Override
    @WebMethod
    @SneakyThrows
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backup.run();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadDataBase64(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.loadDataBase64();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveDataBase64(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.saveDataBase64();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadDataBin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.loadDataBin();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveDataBin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.saveDataBin();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadDataJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.loadDataJson();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveDataJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.saveDataJson();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadDataXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.loadDataXml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveDataXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.saveDataXml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadDataYaml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.loadDataYaml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveDataYaml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.saveDataYaml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadDataJsonJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.loadDataJsonJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveDataJsonJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.saveDataJsonJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadDataXmlJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.loadDataXmlJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveDataXmlJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        backupService.saveDataXmlJaxB();
    }

}
