package ru.vpavlova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.api.service.dto.IProjectService;
import ru.vpavlova.tm.dto.Project;
import ru.vpavlova.tm.enumerated.Sort;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.exception.empty.EmptyDescriptionException;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyNameException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.ProjectNotFoundException;
import ru.vpavlova.tm.exception.system.IndexIncorrectException;
import ru.vpavlova.tm.repository.dto.IProjectRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    public IProjectRepository projectRepository;

    @NotNull
    public IProjectRepository getRepository() {
        return projectRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final Project entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity);
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Project> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        entities.forEach(projectRepository::save);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Project entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.deleteById(entity.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        return Optional.of(projectRepository.getOne(id));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        return projectRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        return projectRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        return Optional.ofNullable(
                projectRepository.findAllByUserId(userId).get(index)
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        return projectRepository.findOneByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final Project entity
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneByIdAndUserId(userId, entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneByIdAndUserId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull Optional<Project> project = findOneByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        projectRepository.removeOneByIdAndUserId(userId, project.get().getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneByUserIdAndName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable String userId, @Nullable String sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<Project> comparator = sortType.getComparator();
        return projectRepository
                .findAllByUserId(userId)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) return;
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.save(entity.get());
    }

}
