package ru.vpavlova.tm.repository.dto;

import ru.vpavlova.tm.api.repository.IRepository;
import ru.vpavlova.tm.dto.Session;

public interface ISessionRepository extends IRepository<Session> {

}

