package ru.vpavlova.tm.api.repository;

import ru.vpavlova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void create();

    void removeById(String id);

    List<Project> findAll();

    Project findById(String id);

    void save(Project project);

    void saveAll(List<Project> list);

    void removeAll();

}
