package ru.vpavlova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vpavlova.tm.api.service.IProjectService;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.model.AuthorizedUser;
import ru.vpavlova.tm.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Secured({"ROLE_USER"})
    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user
    ) {
        projectService.create(user.getUserId());
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        projectService.removeById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        return new ModelAndView(
                "project-edit",
                "command", projectService.findById(user.getUserId(), id)
        );
    }

    @Secured({"ROLE_USER"})
    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @ModelAttribute("project") Project project
    ) {
        projectService.add(user.getUserId(), project);
        return "redirect:/projects";
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "PROJECT EDIT";
    }

}